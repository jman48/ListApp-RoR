<h1>ListApp</h1>

<h3>The problem</h3>
Me and my partner could never get a simple grocery list in sync. 
Every time we went to the supermarket we would forget to take a list or to have written stuff on the list.

<h3>The solution</h3>
A simple online list app that we can both use.

<h3>The code</h3>
The server side of the list app is written in Ruby using Ruby on Rails as the framework. 
It then uses a javascript library that i wrote for it to have a single page web app.

<h3>The why</h3>
I choose to create this system as i thought it would be good practice at writing production type code. 
Also i love to code so it was a good excuse!

<h3>The future</h3>
Will create an android app for this when i get the time



<p>By John Armstrong</p>

