class Item < ActiveRecord::Base
  belongs_to :List
  validates :order, numericality: { only_integer: true }, uniqueness: true
  validates :name, :order, :list, presence: true
end
